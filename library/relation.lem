(******************************************************************************)
(* A library for binary relations                                             *)
(******************************************************************************)

(* ========================================================================== *)
(* Header                                                                     *)
(* ========================================================================== *)

declare {isabelle;ocaml;hol;coq} rename module = lem_relation

open import Bool Basic_classes Tuple Set
open import {hol} `set_relationTheory`

(* ========================================================================== *)
(* The type of relations                                                      *)
(* ========================================================================== *)

type rel_pred 'a 'b = 'a -> 'b -> bool
type rel_set 'a 'b = set ('a * 'b)

(* Binary relations are usually represented as either
   sets of pairs (rel_set) or as curried functions (rel_pred). 
   
   The choice depends on taste and the backend. Lem should not take a 
   decision, but supports both representations. There is an abstract type
   pred, which can be converted to both representations. The representation
   of pred itself then depends on the backend. However, for the time beeing,
   let's implement relations as sets to get them working more quickly. *)

type rel 'a 'b = rel_set 'a 'b

val relToSet : forall 'a 'b. SetType 'a, SetType 'b => rel 'a 'b -> rel_set 'a 'b
val relFromSet : forall 'a 'b. SetType 'a, SetType 'b => rel_set 'a 'b -> rel 'a 'b

let inline relToSet s = s
let inline relFromSet r = r

val relEq : forall 'a 'b. SetType 'a, SetType 'b => rel 'a 'b -> rel 'a 'b -> bool
let relEq r1 r2 = (relToSet r1 = relToSet r2)

(*
instance forall 'a 'b. SetType 'a, SetType 'b => (Eq (rel 'a 'b))
  let (=) = relEq
end
*)

lemma relToSet_inv: (forall r. relFromSet (relToSet r) = r)

val relToPred : forall 'a 'b. SetType 'a, SetType 'b, Eq 'a, Eq 'b => rel 'a 'b -> rel_pred 'a 'b
val relFromPred : forall 'a 'b. SetType 'a, SetType 'b, Eq 'a, Eq 'b => set 'a -> set 'b -> rel_pred 'a 'b -> rel 'a 'b

let relToPred r = (fun x y -> (x, y) IN relToSet r)
let relFromPred xs ys p = { (x, y) | forall (x IN xs) (y IN ys) | p x y }


(* ========================================================================== *)
(* Basic Operations                                                           *)
(* ========================================================================== *)

(* ----------------------- *)
(* Identity relation       *)
(* ----------------------- *)

val inRel : forall 'a 'b. SetType 'a, SetType 'b, Eq 'a, Eq 'b => 'a -> 'b -> rel 'a 'b -> bool
let inline inRel a b rel = (a, b) IN relToSet rel

lemma inRel_set : (forall s a b. inRel a b (relFromSet s) = ((a, b) IN s))
lemma inRel_pred : (forall p a b sa sb. inRel a b (relFromPred sa sb p) = p a b && a IN sa && b IN sb)


(* ----------------------- *)
(* empty relation          *)
(* ----------------------- *)

val relEmpty : forall 'a 'b. SetType 'a, SetType 'b => rel 'a 'b
let inline relEmpty = relFromSet {}

(* ----------------------- *)
(* add relation            *)
(* ----------------------- *)

val relAdd : forall 'a 'b. SetType 'a, SetType 'b => 'a -> 'b -> rel 'a 'b -> rel 'a 'b
let inline relAdd a b r = relFromSet (insert (a,b) (relToSet r))


(* ----------------------- *)
(* Identity relation        *)
(* ----------------------- *)

val relIdOn : forall 'a. SetType 'a, Eq 'a => set 'a -> rel 'a 'a
let relIdOn s = relFromPred s s (=)

val relId : forall 'a. SetType 'a, Eq 'a => rel 'a 'a
let ~{coq;ocaml} relId = {(x, x) | forall x | true}

lemma relId_spec: (forall x y s. (inRel x y (relIdOn s) <-> (x IN s && (x = y))))


(* ----------------------- *)
(* relation union          *)
(* ----------------------- *)

val relUnion : forall 'a 'b. SetType 'a, SetType 'b => rel 'a 'b -> rel 'a 'b -> rel 'a 'b
let inline relUnion r1 r2 = relFromSet ((relToSet r1) union (relToSet r2))

(* ----------------------- *)
(* relation intersection   *)
(* ----------------------- *)

val relIntersection : forall 'a 'b. SetType 'a, SetType 'b, Eq 'a, Eq 'b => rel 'a 'b -> rel 'a 'b -> rel 'a 'b
let inline relIntersection r1 r2 = relFromSet ((relToSet r1) inter (relToSet r2))


(* ----------------------- *)
(* Relation Composition    *)
(* ----------------------- *)

val relComp : forall 'a 'b 'c. SetType 'a, SetType 'b, SetType 'c, Eq 'a, Eq 'b => rel 'a 'b -> rel 'b 'c -> rel 'a 'c
let relComp r1 r2 = relFromSet {(e1, e3) | forall ((e1,e2) IN (relToSet r1)) ((e2',e3) IN (relToSet r2)) | e2 = e2'}

declare hol target_rep function relComp = `rcomp`

lemma rel_comp_1 : (forall r1 r2 e1 e2 e3. (inRel e1 e2 r1 && inRel e2 e3 r2) --> inRel e1 e3 (relComp r1 r2))
lemma ~{coq;ocaml} rel_comp_2 : (forall r. (relComp r relId = r) && (relComp relId r = r))
lemma rel_comp_3 : (forall r. (relComp r relEmpty = relEmpty) && (relComp relEmpty r = relEmpty))


(* ----------------------- *)
(* restrict                *)
(* ----------------------- *)

val relRestrict : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> rel 'a 'a
let relRestrict r s = relFromSet ({ (a, b) | forall (a IN s) (b IN s) | inRel a b r })

declare hol target_rep function relRestrict = `rrestrict`

(* ----------------------- *)
(* Converse                *)
(* ----------------------- *)

val relConverse : forall 'a 'b. SetType 'a, SetType 'b => rel 'a 'b -> rel 'b 'a
let relConverse r = relFromSet (Set.map swap (relToSet r))

(* ----------------------- *)
(* domain                  *)
(* ----------------------- *)

val relDomain : forall 'a 'b. SetType 'a, SetType 'b => rel 'a 'b -> set 'a
let relDomain r = Set.map fst (relToSet r)

declare hol target_rep function relDomain = `domain`

(* ----------------------- *)
(* range                   *)
(* ----------------------- *)

val relRange : forall 'a 'b. SetType 'a, SetType 'b => rel 'a 'b -> set 'b
let relRange r = Set.map snd (relToSet r)

declare hol target_rep function relRange = `range`


(* ----------------------- *)
(* field / definedOn       *)
(*                         *)
(* avoid the keyword field *)
(* ----------------------- *)

val relDefinedOn : forall 'a. SetType 'a => rel 'a 'a -> set 'a
let inline relDefinedOn r = ((relDomain r) union (relRange r))

declare {hol} rename function relDefinedOn = rdefined_on


(* ----------------------- *)
(* relOver                 *)
(*                         *)
(* avoid the keyword field *)
(* ----------------------- *)

val relOver : forall 'a. SetType 'a => rel 'a 'a -> set 'a -> bool
let inline relOver r s = (forall ((a,b) IN r). a IN s && b IN s)

declare {hol} rename function relOver = rel_over


(* ----------------------- *)
(* apply a relation        *)
(* ----------------------- *)

(* Given a relation r and a set s, relApply r s applies s to r, i.e.
   it returns the set of all value reachable via r form a value in s.
   This operation can be seen as a generalisation of function application. *)
   
val relApply : forall 'a 'b. SetType 'a, SetType 'b, Eq 'a => rel 'a 'b -> set 'a -> set 'b
let relApply r s = { y | forall ((x, y) IN (relToSet r)) | x IN s }
declare {hol} rename function relApply = rapply


(* ========================================================================== *)
(* Properties                                                                 *)
(* ========================================================================== *)

(* ----------------------- *)
(* subrel                  *)
(* ----------------------- *)

val isSubrel : forall 'a 'b. SetType 'a, SetType 'b, Eq 'a, Eq 'b => rel 'a 'b -> rel 'a 'b -> bool
let inline isSubrel r1 r2 = isSubsetOf (relToSet r1) (relToSet r2)


(* ----------------------- *)
(* reflexivity             *)
(* ----------------------- *)

val isReflexiveOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isReflexiveOn r s = (forall (e IN s). inRel e e r)

declare {hol} rename function isReflexiveOn = lem_is_reflexive_on

val isReflexive : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isReflexive r = (forall e. inRel e e r)

declare {hol} rename function isReflexive = lem_is_reflexive


(* ----------------------- *)
(* irreflexivity           *)
(* ----------------------- *)

val isIrreflexiveOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isIrreflexiveOn r s = (forall (e IN s). not (inRel e e r))

declare hol target_rep function isIrreflexiveOn = `irreflexive`

val isIrreflexive : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let isIrreflexive r = (forall ((e1, e2) IN (relToSet r)). not (e1 = e2))

declare {hol} rename function isIrreflexive = lem_is_irreflexive


(* ----------------------- *)
(* symmetry                *)
(* ----------------------- *)

val isSymmetricOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isSymmetricOn r s = (forall (e1 IN s) (e2 IN s). (inRel e1 e2 r) --> (inRel e2 e1 r))

declare {hol} rename function isSymmetricOn = lem_is_symmetric_on

val isSymmetric : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let isSymmetric r = (forall ((e1, e2) IN relToSet r). inRel e2 e1 r)

declare {hol} rename function isSymmetric = lem_is_symmetric


(* ----------------------- *)
(* antisymmetry            *)
(* ----------------------- *)

val isAntisymmetricOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isAntisymmetricOn r s = (forall (e1 IN s) (e2 IN s). (inRel e1 e2 r) --> (inRel e2 e1 r) --> (e1 = e2))

declare {hol} rename function isAntisymmetricOn = lem_is_antisymmetric_on

val isAntisymmetric : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let isAntisymmetric r = (forall ((e1, e2) IN relToSet r). (inRel e2 e1 r) --> (e1 = e2))

declare hol target_rep function isAntisymmetric = `antisym`


(* ----------------------- *)
(* transitivity            *)
(* ----------------------- *)

val isTransitiveOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isTransitiveOn r s = (forall (e1 IN s) (e2 IN s) (e3 IN s). (inRel e1 e2 r) --> (inRel e2 e3 r) --> (inRel e1 e3 r))

declare {hol} rename function isTransitiveOn = lem_transitive_on

val isTransitive : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let isTransitive r = (forall ((e1, e2) IN relToSet r) (e3 IN relApply r {e2}). inRel e1 e3 r)

declare hol target_rep function isTransitive = `transitive`


(* ----------------------- *)
(* total                   *)
(* ----------------------- *)

val isTotalOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isTotalOn r s = (forall (e1 IN s) (e2 IN s). (inRel e1 e2 r) || (inRel e2 e1 r))

declare {hol} rename function isTotalOn = lem_is_total_on


val isTotal : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isTotal r = (forall e1 e2. (inRel e1 e2 r) || (inRel e2 e1 r))
declare {hol} rename function isTotal = lem_is_total


val isTrichotomousOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isTrichotomousOn r s = (forall (e1 IN s) (e2 IN s). (inRel e1 e2 r) || (e1 = e2) || (inRel e2 e1 r))

declare {hol} rename function isTrichotomousOn = lem_is_trichotomous_on

val isTrichotomous : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isTrichotomous r = (forall e1 e2. (inRel e1 e2 r) || (e1 = e2) || (inRel e2 e1 r))

declare {hol} rename function isTrichotomous = lem_is_trichotomous


(* ----------------------- *)
(* is_single_valued        *)
(* ----------------------- *)

val isSingleValued : forall 'a 'b. SetType 'a, SetType 'b, Eq 'a, Eq 'b => rel 'a 'b -> bool
let isSingleValued r = (forall ((e1, e2a) IN relToSet r) (e2b IN relApply r {e1}). e2a = e2b) 

declare {hol} rename function isSingleValued = lem_is_single_valued


(* ----------------------- *)
(* equivalence relation    *)
(* ----------------------- *)

val isEquivalenceOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isEquivalenceOn r s = isReflexiveOn r s && isSymmetricOn r s && isTransitiveOn r s

declare {hol} rename function isEquivalenceOn = lem_is_equivalence_on


val isEquivalence : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isEquivalence r = isReflexive r && isSymmetric r && isTransitive r

declare {hol} rename function isEquivalence = lem_is_equivalence


(* ----------------------- *)
(* well founded            *)
(* ----------------------- *)

val isWellFounded : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isWellFounded r = (forall P. (forall x. (forall y. inRel y x r --> P x) --> P x) --> (forall x. P x))

declare hol      target_rep function isWellFounded r = `WF` (`reln_to_rel` r)


(* ========================================================================== *)
(* Orders                                                                     *)
(* ========================================================================== *)


(* ----------------------- *)
(* pre- or quasiorders     *)
(* ----------------------- *)

val isPreorderOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isPreorderOn r s = isReflexiveOn r s && isTransitiveOn r s

declare {hol} rename function isPreorderOn = lem_is_preorder_on

val isPreorder : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isPreorder r = isReflexive r && isTransitive r

declare {hol} rename function isPreorder = lem_is_preorder


(* ----------------------- *)
(* partial orders          *)
(* ----------------------- *)

val isPartialOrderOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isPartialOrderOn r s = isReflexiveOn r s && isTransitiveOn r s && isAntisymmetricOn r s

declare {hol} rename function isPartialOrderOn = lem_is_partial_order_on

val isStrictPartialOrderOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isStrictPartialOrderOn r s = isIrreflexiveOn r s && isTransitiveOn r s

declare hol target_rep function isStrictPartialOrderOn = `lem_is_strict_partial_order_on`

val isStrictPartialOrder : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let isStrictPartialOrder r = isIrreflexive r && isTransitive r 

declare hol target_rep function isStrictPartialOrder = `lem_is_strict_partial_order`

val isPartialOrder : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isPartialOrder r = isReflexive r && isTransitive r && isAntisymmetric r

declare hol target_rep function isPartialOrder = `lem_is_partial_order`

(* ----------------------- *)
(* total / linear orders   *)
(* ----------------------- *)

val isTotalOrderOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isTotalOrderOn r s = isPartialOrderOn r s && isTotalOn r s

declare hol target_rep function isTotalOrderOn = `lem_is_total_order_on`

val isStrictTotalOrderOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> bool
let isStrictTotalOrderOn r s = isStrictPartialOrderOn r s && isTrichotomousOn r s

declare hol target_rep function isStrictTotalOrderOn = `lem_is_strict_total_order_on`

val isTotalOrder : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isTotalOrder r = isPartialOrder r && isTotal r 

declare hol target_rep function isTotalOrder = `lem_is_total_order`

val isStrictTotalOrder : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> bool
let ~{ocaml;coq} isStrictTotalOrder r = isStrictPartialOrder r && isTrichotomous r 

declare hol target_rep function isStrictTotalOrder = `lem_is_strict_total_order`



(* ========================================================================== *)
(* closures                                                                   *)
(* ========================================================================== *)

(* ----------------------- *)
(* transitive closure      *)
(* ----------------------- *)

val transitiveClosure : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> rel 'a 'a
val transitiveClosureByEq  : forall 'a. ('a -> 'a -> bool) -> rel 'a 'a -> rel 'a 'a
val transitiveClosureByCmp : forall 'a. ('a * 'a -> 'a * 'a -> ordering) -> rel 'a 'a -> rel 'a 'a

declare ocaml    target_rep function transitiveClosureByCmp = `Pset.tc`
declare hol      target_rep function transitiveClosure = `tc`
declare isabelle target_rep function transitiveClosure = `trancl`
declare coq      target_rep function transitiveClosureByEq = `set_tc`

let inline {coq} transitiveClosure = transitiveClosureByEq (=)
let inline {ocaml} transitiveClosure = transitiveClosureByCmp setElemCompare


lemma transitiveClosure_spec1: (forall r. isSubrel r (transitiveClosure r))
lemma transitiveClosure_spec2: (forall r. isTransitive (transitiveClosure r))
lemma transitiveClosure_spec3: (forall r1 r2. ((isTransitive r2) && (isSubrel r1 r2)) --> isSubrel (transitiveClosure r1) r2)


(* ========================================================================== *)
(* reflexiv closures                                                          *)
(* ========================================================================== *)

val reflexivTransitiveClosureOn : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> set 'a -> rel 'a 'a
let reflexivTransitiveClosureOn r s = transitiveClosure (relUnion r (relIdOn s))

val reflexivTransitiveClosure : forall 'a. SetType 'a, Eq 'a => rel 'a 'a -> rel 'a 'a
let ~{ocaml;coq} reflexivTransitiveClosure r = transitiveClosure (relUnion r relId)

